<?php

$databases = array (
  'default' => array (
    'default' => array (
      'driver' => 'mysql',
      'database' => 'crc_admin',
      'username' => 'crcadmin',
      'password' => '3jf8e39Q',
      //'password' => 'admin',
      'host' => '127.0.0.1',
      //'host' => 'localhost',
      'port' => '3306',
    ),
  ),
  'crcdid' => array (
    'default' => array (
      'driver' => 'mysql',
      'database' => 'crc_dids',
      'username' => 'crcadmin',
      'password' => '3jf8e39Q',
      //'password' => 'admin',
      'host' => 'localhost',
      //'host' => '127.0.0.1',
      //'host' => '52.90.43.16',
      'port' => '3306',
    ),
  ),
);
$db_prefix = '';

$update_free_access = FALSE;

/*$base_url = 'http://admin.jaguarfax.com';
$cookie_domain = '.admin.jaguarfax.com';
*/
ini_set('session.gc_probability', 1);
ini_set('session.gc_divisor', 100);
ini_set('session.gc_maxlifetime', 200000);
ini_set('session.cookie_lifetime', 2000000);

//$conf['https'] = TRUE;
$conf['file_public_path'] = 'sites/admin/files';
$conf['file_private_path'] = 'sites/admin/private';
$conf['file_temporary_path'] = '/tmp';

/*$conf['syslog_identity'] = 'drupal_admin.jaguarfax.com';*/
$conf['mail_system'] = array(
   'default-system' => 'CRC_SmtpMailSystem',
);