<?php
$aliases = array();
$drupal = '/var/www/drupal';

// Automatic alias for each Drupal site
$site = new DirectoryIterator($drupal . '/sites');
while ($site->valid()) {
  // Look for directories containing a 'settings.php' file
  if ($site->isDir() && !$site->isDot() && !$site->isLink()) {
    if (file_exists($site->getPathname() . '/settings.php')) {
      // Add site alias
      $basename = $site->getBasename();
      $aliases[$basename] = array(
        'uri' => $basename,
        'root' => $drupal,
      );
    }
  }
  $site->next();
}

// Get all site aliases
$all = array();
foreach ($aliases as $name => $definition) {
  $all[] = '@' . $name;
}

// 'All' alias group
$aliases['all'] = array(
  'site-list' => $all,
);

?>
