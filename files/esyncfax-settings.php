<?php

$databases = array (
  'default' => array (
    'default' => array (
      'driver' => 'mysql',
      'database' => 'crc_customer_esyncfax',
      'username' => 'crcadmin',
      'password' => '3jf8e39Q',
      'charset' => 'utf8mb4',
      'collation' => 'utf8mb4_general_ci',
      'host' => 'localhost',
      'port' => '3306',
    ),
  ),

  // To be switched to remote IP of admin portal instance
  'crcdid' => array (
    'default' => array (
      'driver' => 'mysql',
      'database' => 'crc_dids',
      'username' => 'crcadmin',
      'password' => '3jf8e39Q',
      'charset' => 'utf8mb4',
      'collation' => 'utf8mb4_general_ci',
      //'host' => '52.90.43.16', //'127.0.0.1', // To be switched to '52.60.69.48'
      'host' => 'localhost',
      'port' => '3306',
    ),
  ),


  // DB for syncing/exposing changed users for admins at the Dashboard
  'crc_sync_dashboard' => array (
    'default' => array (
      'driver' => 'mysql',
      'database' => 'crc_admin',
      'username' => 'crcadmin',
      'password' => '3jf8e39Q',
      'charset' => 'utf8mb4',
      'collation' => 'utf8mb4_general_ci',
      'host' => '127.0.0.1', // local, admin.dev2.esncfax.com
      //'host' => '52.90.43.16', // admin.esyncfax.com
      //'host' => '52.60.69.48', // admin.callmom.net
      'port' => '3306',
    ),
  ),

  // Local crcdid that remains to keep local auth data
  'crc_auth_local' => array (
    'default' => array (
      'driver' => 'mysql',
      'database' => 'crc_dids',
      'username' => 'crcadmin',
      'password' => '3jf8e39Q',
      'charset' => 'utf8mb4',
      'collation' => 'utf8mb4_general_ci',
      'host' => 'localhost',
      'port' => '3306',
    ),
  ),

 // Test remote crcdid DB, just for testing remote access
 'crcdid_remote' => array (
    'default' => array (
      'driver' => 'mysql',
      'database' => 'crc_dids',
      'username' => 'crcadmin',
      'password' => '3jf8e39Q',
      'charset' => 'utf8mb4',
      'collation' => 'utf8mb4_general_ci',
      //'host' => '52.90.43.16', //'127.0.0.1', // To be switched to '52.60.69.48'
      'host' => 'localhost',
      'port' => '3306',
    ),
  ),

  // Remote Amazon RDS MySql DB for synchronization with the old ESB sites
  'crc_sync_remote' => array (
    'default' => array (
      'driver' => 'mysql',
      'database' => 'replication_queue',
      'username' => 'crcadmin',
      'password' => 'TeYavaTH58ru',
      'host' => 'aws-rds-01.cs58lmeebk57.us-east-1.rds.amazonaws.com',
      'port' => '3306',
    ),
  ),



);
$db_prefix = '';

$update_free_access = FALSE;

/*$base_url = 'https://www.esyncfax.com';
$cookie_domain = 'www.esyncfax.com';
*/
ini_set('session.gc_probability', 1);
ini_set('session.gc_divisor', 100);
ini_set('session.gc_maxlifetime', 200000);
ini_set('session.cookie_lifetime', 2000000);

/*$conf['https'] = TRUE;*/
$conf['file_public_path'] = 'sites/esyncfax/files';
$conf['file_private_path'] = 'sites/esyncfax/private';
$conf['file_temporary_path'] = '/tmp';

/*$conf['syslog_identity'] = 'drupal_.esyncfax.com';*/

$conf['site_name'] = "Esync Fax Portal";
$conf['site_mail'] = "no-reply@esyncfax.com";
$conf['mimemail_from'] = "no-reply@esyncfax.com";
$conf['smtp_from'] = "no-reply@esyncfax.com";
$conf['smtp_fromname'] = "Esync Fax Portal";
$conf['crc_admin_domain'] = "admin.esyncfax.com";
$conf['crc_admin_domain_scheme'] = "http";

$conf['crc_mailhandler_mailbox'] = array("name" => "no-reply@esyncfax.com");
$conf['crc_mailhandler_disabled'] = FALSE;
$conf['crc_mode_self_signup'] = 0;
$conf['crc_mailimport_accept_anonymous'] = FALSE;
$conf['crc_faxnumber_required'] = TRUE;
$conf['twilio_number'] = "";
$conf['amazons3_bucket'] = "esyncfax";
$conf['amazons3_key'] = "AKIAIWC6NXLMTO7ATOHQ";
$conf['amazons3_secret'] = "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";


$conf['mailsystem']['default-system'] = "DevelMailLog";//[default-system]

// Connection 1 (CRC Int...)
#$conf['aerialink_apiKey'] = '41f631f1c3098930e88748036112abc0';
#$conf['aerialink_apiSecret'] = '3cf01e3cac4d9192eeea636425e1b71b';

// Connection 2 (CRC-Endpoint-2)
$conf['aerialink_apiKey'] = '361ff643fce79e027920e85798f89e2e';
$conf['aerialink_apiSecret'] = '5d05803f2e4b9ff86009b60dfc6bcabd';

// Connection 2-2 (CRC-Endpoint-2)
#$conf['aerialink_apiKey'] = '984610e655c69aa3a64da00cf786004e';
#$conf['aerialink_apiSecret'] = 'a396ccb2aedfbe232ba42f3815dd77e5';



$conf['gauth_login_client_id'] = '614284697696-isfprodh4n136op04srvft0erdn0mi9p.apps.googleusercontent.com';
$conf['gauth_login_client_secret'] = 'AcXnHIF-bs0cRzzSQsHIttTp';


$conf['views_data_export_gc_expires'] = 600;

ini_set('display_errors', TRUE);
ini_set('display_startup_errors', TRUE);
$conf['theme_debug'] = FALSE;
$conf['composer_manager_file_dir'] = 'sites/esyncfax/files/composer';
//$conf['googleanalytics_account'] = 'UA-74052728-2';
$conf['mail_system'] = array(
   'default-system' => 'CRC_SmtpMailSystem',
);